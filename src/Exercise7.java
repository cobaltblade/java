import java.util.Scanner;

/*Description:	Create a class called 'Matrix' containing constructor that initializes the number of rows and
number of columns of a new Matrix object. The Matrix class has the following information:
1 - number of rows of matrix
2 - number of columns of matrix
3 - elements of matrix in the form of 2D array

Author: Sakib Muhtadee
*/
class Matrix
{
	int row,column;
	int arr[][]=new int[row][column];
	public void Matrix(int r,int c)
	{
		row=r;
		column=c;

	}
	public void getMatrix()
	{
		int r=this.row;
		int c=this.column;
		int i,j;
		System.out.println("Enter Elements of Matrix:");
		Scanner S = new Scanner(System.in);
		for (i=1;i<=r;i++)
		{

			for (j=1;j<=c;j++)
			{				arr[i][j]=S.nextInt();
			}
		}
	}
	public void showMatrix()
	{
		int r=this.row;
		int c=this.column;
		int i,j;
		System.out.println("The Elements of Matrix are:");
		Scanner S = new Scanner(System.in);
		for (i=1;i<=r;i++)
		{
			System.out.println(" ");
			for (j=1;j<=c;j++)
			{
				System.out.print(arr[i][j]+" ");
			}
		}
	}

}
public class Exercise7 {
	public static void main(String[] args)
	{
		Matrix M=new Matrix();
		M.Matrix(3, 4);
		M.getMatrix();
		M.showMatrix();

	}

}
