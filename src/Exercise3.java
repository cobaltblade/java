/*
 	Problem:	Write a program to print the area of a rectangle by creating a class named 'Area' taking the
				values of its length and breadth as parameters of its constructor and having a method named
				'returnArea' which returns the area of the rectangle. Length and breadth of rectangle are entered
				through keyboard.
	Author :	Md. Sakib Muhtadee
 */
import java.util.Scanner;

public class Exercise3 {
	public static void main(String[] args)
	{
		Rectangle2 r=new Rectangle2();
		int w,h,ans;
		Scanner S = new Scanner(System.in);
		System.out.println("Enter width:");
		w=S.nextInt();
		System.out.println("Enter height:");
		h=S.nextInt();
		
		ans=r.Area(w,h);
		System.out.println("Area:"+ans);
	}
}
class Rectangle2 {
	public static int Area(int w,int h)
	{
	   int area=w*h;
	   return area;
	}
}
