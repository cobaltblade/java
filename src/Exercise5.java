import java.util.Scanner;

/*Description:	Write a program by creating an 'Employee' class having the following methods and print the final
                salary.
                1 - 'getInfo()' which takes the salary, number of hours of work per day of employee as parameter
                2 - 'AddSal()' which adds $10 to salary of the employee if it is less than $500.
                3 - 'AddWork()' which adds $5 to salary of employee if the number of hours of work per day is
                     more than 6 hours.

Author: Md Sakib Muhtadee
Properties of Encapsulation:
1) values of the class must be private
2) Must have a getter() method.
3) Must have a setter() method.
*/
class Employee
{
	private int salary,w_hours;
	public void getInfo(int salary,int w_hours)
	{
		this.salary=salary;
		this.w_hours=w_hours;

		if((this.salary*this.w_hours)<500)
		{
			this.AddSal();
		}
		if(this.w_hours<6)
		{
			this.AddWork();
		}
	}
	public void AddSal()
	{
		this.salary=this.salary+10;
	}
	public void AddWork()
	{
		this.salary=this.salary+5;
	}
	public void Print()
	{
		System.out.println("Hourly Salary:"+this.salary+"\n");
		System.out.println("Work Hour:"+this.w_hours+"\n");
		System.out.println("Total Salary:"+(this.salary*this.w_hours)+"\n");
	}


}
public class Exercise5 {
	public static void main(String[] args)
	{
		Employee E=new Employee();
		Scanner S = new Scanner(System.in);
		int salary=S.nextInt();
		int w_hour=S.nextInt();
		E.getInfo(salary, w_hour);
		E.Print();
	}
}
