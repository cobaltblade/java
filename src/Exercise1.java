/*
 	Problem:	Write a program to print the area and perimeter of a triangle having sides of 3, 4 and 5 units by
				creating a class named 'Triangle' without any parameter in its constructor.
	Author :	Md. Sakib Muhtadee
 */
public class Exercise1 
{
	public static void main(String[] args)
	{
		Triangle t=new Triangle();
		t.solution(3,4,5);
	}
}

class Triangle
{
	public static void solution(int a, int b,int c)
	{
		double perimeter=(a+b+c);
		double s=perimeter/2;
	    double area=Math.sqrt(s*(s-a)*(s-b)*(s-c));
	    System.out.println("Area		: "+area);
	    System.out.println("Perimeter	: "+perimeter);
	}
}
