import java.util.Scanner;

/*
 	Problem:	Print the average of three numbers entered by user by creating a class named 'Average' having a
				method to calculate and print the average.
	Author :	Md. Sakib Muhtadee
 */
public class Exercise4 {
	public static void main(String[] args)
	{
		Average a=new Average();
		int x,y,z;
		Scanner S = new Scanner(System.in);
		x=S.nextInt();
		
		y=S.nextInt();
		
		z=S.nextInt();
		
		a.print(x, y, z);
	}
}
class Average{
	public static int calculate(int a,int b,int c)
	{
		return (a+b+c)/3;
	}
	
	public static void print(int a,int b,int c)
	{
		System.out.println("Average:	"+calculate(a,b,c));
	}
}