/*
 	Problem:	Write a program to print the area of two rectangles having sides (4,5) and (5,8) respectively by
				creating a class named 'Rectangle' with a method named 'Area' which returns the area and length
				and breadth passed as parameters to its constructor.
	Author :	Md. Sakib Muhtadee
 */
public class Exercise2 {
	public static void main(String[] args)
	{
		Rectangle R1=new Rectangle();
		Rectangle R2=new Rectangle();
		
		System.out.println("R1 Area: "+R1.Area(4, 5));
		System.out.println("R2 Area: "+R2.Area(5, 8));
		
	}
}

class Rectangle
{
	public static int Area(int w,int h)
	{
	   int area=w*h;
	   return area;
	}
}
